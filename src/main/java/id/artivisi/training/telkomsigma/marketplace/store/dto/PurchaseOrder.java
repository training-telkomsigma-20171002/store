package id.artivisi.training.telkomsigma.marketplace.store.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class PurchaseOrder {
    private Date waktuTransaksi = new Date();
    private List<CartItem> daftarBelanja = new ArrayList<>();
    private Shipment pengiriman = new Shipment();
}
